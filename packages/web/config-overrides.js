const rewireAliases = require('react-app-rewire-aliases');
const fs = require('fs');
const path = require('path');
const webpack = require('webpack');

const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

const appIncludes = [
  resolveApp('src'),
  resolveApp('../common/src')
];

module.exports = function override(config, env) {

  // config = rewireAliases.aliasesOptions({
  //   'react-native-web': path.resolve(__dirname, 'node_modules', 'react-native-web')
  // })(config, env);

  const babel = config.module.rules
    .find(rule => 'oneOf' in rule).oneOf
    .find(rule => /babel-loader/.test(rule.loader));

  if (!Array.isArray(babel.include)) {
    babel.include = [babel.include];
  }

  babel.include = babel.include.concat([
    path.resolve(__dirname, '..', 'common')
  ]);

  babel.options.plugins = babel.options.plugins.concat([
    require.resolve('babel-plugin-react-native-web')
  ]);

  // config.resolve.plugins = config.resolve.plugins.filter(
  //   plugin => plugin.constructor.name !== 'ModuleScopePlugin'
  // );

  // config.module.rules[0].include = appIncludes;
  // config.module.rules[1] = null
  // config.module.rules[2].oneOf[1].include = appIncludes;
  // config.module.rules = config.module.rules.filter(Boolean);
  config.plugins.push(
    new webpack.DefinePlugin({ __DEV__: env !== 'production' })
  );

  return config;
};
