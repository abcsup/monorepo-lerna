import { AppRegistry } from 'react-native';

import App from 'common/src/App';

AppRegistry.registerComponent('monorepo', () => App);
AppRegistry.runApplication('monorepo', {
  rootTag: document.getElementById('root')
});
